
package com.pakupaku.owner.model.ordersmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Invoice {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("paid")
    @Expose
    private Integer paid;
    @SerializedName("gross")
    @Expose
    private Integer gross;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("delivery_charge")
    @Expose
    private Integer deliveryCharge;
    @SerializedName("restaurant_or_packing_charges")
    @Expose
    private Integer restaurantOrPackingCharges;
    @SerializedName("wallet_amount")
    @Expose
    private Integer walletAmount;
    @SerializedName("promocode_id")
    @Expose
    private Integer promocodeId;
    @SerializedName("promocode_amount")
    @Expose
    private Integer promocodeAmount;
    @SerializedName("payable")
    @Expose
    private Integer payable;
    @SerializedName("tax")
    @Expose
    private Integer tax;
    @SerializedName("net")
    @Expose
    private Integer net;
    @SerializedName("total_pay")
    @Expose
    private Integer totalPay;
    @SerializedName("tender_pay")
    @Expose
    private Integer tenderPay;
    @SerializedName("ripple_price")
    @Expose
    private String ripplePrice;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("DestinationTag")
    @Expose
    private String destinationTag;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public Integer getGross() {
        return gross;
    }

    public void setGross(Integer gross) {
        this.gross = gross;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(Integer deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public Integer getRestaurantOrPackingCharges() {
        return restaurantOrPackingCharges;
    }

    public void setRestaurantOrPackingCharges(Integer restaurantOrPackingCharges) {
        this.restaurantOrPackingCharges = restaurantOrPackingCharges;
    }

    public Integer getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(Integer walletAmount) {
        this.walletAmount = walletAmount;
    }

    public Integer getPromocodeId() {
        return promocodeId;
    }

    public void setPromocodeId(Integer promocodeId) {
        this.promocodeId = promocodeId;
    }

    public Integer getPromocodeAmount() {
        return promocodeAmount;
    }

    public void setPromocodeAmount(Integer promocodeAmount) {
        this.promocodeAmount = promocodeAmount;
    }

    public Integer getPayable() {
        return payable;
    }

    public void setPayable(Integer payable) {
        this.payable = payable;
    }

    public Integer getTax() {
        return tax;
    }

    public void setTax(Integer tax) {
        this.tax = tax;
    }

    public Integer getNet() {
        return net;
    }

    public void setNet(Integer net) {
        this.net = net;
    }

    public Integer getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(Integer totalPay) {
        this.totalPay = totalPay;
    }

    public Integer getTenderPay() {
        return tenderPay;
    }

    public void setTenderPay(Integer tenderPay) {
        this.tenderPay = tenderPay;
    }

    public String getRipplePrice() {
        return ripplePrice;
    }

    public void setRipplePrice(String ripplePrice) {
        this.ripplePrice = ripplePrice;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getDestinationTag() {
        return destinationTag;
    }

    public void setDestinationTag(String destinationTag) {
        this.destinationTag = destinationTag;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
