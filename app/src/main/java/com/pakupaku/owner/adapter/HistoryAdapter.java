package com.pakupaku.owner.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pakupaku.owner.R;
import com.pakupaku.owner.activity.OrderDetailsActivity;
import com.pakupaku.owner.helper.GlobalData;
import com.pakupaku.owner.model.ordersmodel.Address;
import com.pakupaku.owner.model.ordersmodel.Invoice;
import com.pakupaku.owner.model.ordersmodel.Orders;
import com.pakupaku.owner.model.ordersmodel.User;
import com.pakupaku.owner.utils.Utils;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {
    private Context context;
    private List<Orders> ordersList;

    public HistoryAdapter(List<Orders> list, Context con) {
        this.ordersList = list;
        this.context = con;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_item_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Orders orders = ordersList.get(position);
        holder.itemLayout.setOnClickListener(v -> {
            GlobalData.selectedOrder = ordersList.get(position);
            Intent intent = new Intent(context, OrderDetailsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
        if (orders.getUser() != null) {
            User user = orders.getUser();
            String name = Utils.toFirstCharUpperAll(user.getName());
            holder.userName.setText(name);
            Glide.with(context).load(user.getAvatar()).apply(new RequestOptions().placeholder(R.drawable.ic_place_holder_image).error(R.drawable.ic_place_holder_image).dontAnimate()).into(holder.userImg);
        }

        if (ordersList.get(position).getAddress() != null) {
            Address address = orders.getAddress();
            holder.address.setText(address.getMapAddress());
        }

        if (orders.getInvoice() != null) {
            Invoice invoice = orders.getInvoice();
            holder.total_price.setText(GlobalData.profile.getCurrency() + invoice.getNet());
            String payment_mode = orders.getInvoice().getPaymentMode();
            payment_mode = payment_mode.substring(0, 1).toUpperCase() + payment_mode.substring(1);
            holder.paymentMode.setText(payment_mode);
        } else {
            holder.paymentMode.setText("Null");
        }

        long dateLong = Utils.getDateInMillis(orders.getCreatedAt());
        String text = Utils.getRelationTime(dateLong);
        holder.times.setText(text);

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView userName;
        TextView address;
        TextView paymentMode;
        TextView total_price;
        TextView times;
        CardView itemLayout;
        RelativeLayout price_layout;
        ImageView userImg;

        public MyViewHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.user_name);
            total_price = view.findViewById(R.id.total_price);
            times = view.findViewById(R.id.timesAgo);
            address = view.findViewById(R.id.address);
            paymentMode = view.findViewById(R.id.payment_mode);
            itemLayout = view.findViewById(R.id.itemLayout);
            price_layout = view.findViewById(R.id.price_layout);
            userImg = view.findViewById(R.id.user_img);
        }
    }
}
