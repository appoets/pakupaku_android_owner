package com.pakupaku.owner.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pakupaku.owner.R;
import com.pakupaku.owner.model.NotificationList;
import com.pakupaku.owner.utils.Utils;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyNotification> {

    private Context context;
    private Activity activity;
    private List<NotificationList> list;

    public NotificationAdapter(List<NotificationList> list, Context con) {
        this.list = list;
        this.context = con;
    }

    @NonNull
    @Override
    public MyNotification onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_notification, parent, false);
        return new MyNotification(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyNotification holder, int position) {
        holder.txtDiscription.setText(list.get(position).getMessage());
        long dateLong = Utils.getDateInMillis(list.get(position).getCreatedAt());
        String text = Utils.getRelationTime(dateLong);
        holder.txtTime.setText(text);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyNotification extends RecyclerView.ViewHolder {
        @BindView(R.id.user_img)
        CircleImageView userImg;
        @BindView(R.id.txt_description_title)
        TextView txtDescriptionTitle;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.txt_discription)
        TextView txtDiscription;

        MyNotification(@NonNull View itemView) {
            super(itemView);
            // ButterKnife.bind(this, itemView);
        }
    }
}
