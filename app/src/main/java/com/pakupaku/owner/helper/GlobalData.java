package com.pakupaku.owner.helper;

import com.pakupaku.owner.model.Profile;
import com.pakupaku.owner.model.ordersmodel.Orders;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Tamil on 12/27/2017.
 */

public class GlobalData {
    public static String accessToken = "";
    public static Profile profile;
    public static Orders selectedOrder;
    public static List<String> ORDER_STATUS = Arrays.asList("ORDERED", "RECEIVED", "ASSIGNED", "PROCESSING", "REACHED", "PICKEDUP", "ARRIVED", "COMPLETED");
}
