package com.pakupaku.owner.utils;

public class Constants {

    public static final String VEG = "veg";
    public static final String NON_VEG = "non-veg";

    public interface BROADCAST {
        String UPDATE_ORDERS = "update_orders";
    }

    public interface PREF {
        String PROFILE_ID = "profile_id";
        String CURRENCY = "currency";
        String HISTORY_TYPE = "history_type";
    }

    public interface HISTORY_TYPE {
        String ACCEPTED = "accepted";
        String SCHEDULED = "scheduled";
        String ONGOING = "ongoing";
        String PENDING = "pending";
        String CANCELED = "cancelled";
    }
}
