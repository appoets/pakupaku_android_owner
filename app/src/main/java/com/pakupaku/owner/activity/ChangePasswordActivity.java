package com.pakupaku.owner.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pakupaku.owner.R;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.model.ChangePassword;
import com.pakupaku.owner.model.ServerError;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Utils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.new_password)
    EditText newPassword;
    @BindView(R.id.txt_new_password)
    EditText txtNewPassword;
    @BindView(R.id.txt_confirm_password)
    EditText txtConfirmPassword;

    Context context;
    Activity activity;
    ConnectionHelper connectionHelper;
    CustomDialog customDialog;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    String TAG = "ChangePasswordActivity";
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.title)
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        context = ChangePasswordActivity.this;
        activity = ChangePasswordActivity.this;
        connectionHelper = new ConnectionHelper(context);
        customDialog = new CustomDialog(context);

        backImg.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.change_password));
    }

    @OnClick(R.id.click_change_pswd)
    public void onViewClicked() {
        changePassword();
    }

    private void changePassword() {
        String strCurrentPassword = newPassword.getText().toString().trim();
        String strPassword = txtNewPassword.getText().toString().trim();
        String strConfirmPassword = txtConfirmPassword.getText().toString().trim();

        if (strCurrentPassword.isEmpty()) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.please_enter_password));
        } else if (strCurrentPassword.length() < 6) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.please_enter_minimum_length_password));
        } else if (strPassword.isEmpty()) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.please_enter_new_password));
        } else if (strPassword.length() < 6 || strCurrentPassword.length() < 6) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.please_enter_minimum_length_password));
        } else if (strConfirmPassword.isEmpty()) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.please_enter_new_password));
        } else if (!strConfirmPassword.equals(strPassword)) {
            Utils.displayMessage(ChangePasswordActivity.this, getResources().getString(R.string.password_and_confirm_password_doesnot_match));
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put("password_old", strCurrentPassword);
            map.put("password", strPassword);
            map.put("password_confirmation", strConfirmPassword);
            callChangePassword(map);
        }
    }

    private void callChangePassword(HashMap<String, String> params) {
        customDialog.show();
        Call<ChangePassword> call = apiInterface.changePassword(params);
        call.enqueue(new Callback<ChangePassword>() {
            @Override
            public void onResponse(Call<ChangePassword> call, Response<ChangePassword> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    Utils.displayMessage(ChangePasswordActivity.this, getString(R.string.password_updated_successfully));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onBackPressed();
                        }
                    }, 1000);
                } else {
                    try {
                        ServerError serverError = new Gson().fromJson(response.errorBody().charStream(), ServerError.class);
                        if (serverError.getError() != null && serverError.getError().contains("incorrect_password")) {
                            Utils.displayMessage(activity, "Incorrect Password");
                        } else {
                            Utils.displayMessage(activity, serverError.getError());
                        }
                        if (response.code() == 401) {
                            context.startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            activity.finish();
                        }
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePassword> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }
}
