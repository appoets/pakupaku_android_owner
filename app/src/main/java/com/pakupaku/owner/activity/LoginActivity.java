package com.pakupaku.owner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pakupaku.owner.R;
import com.pakupaku.owner.config.AppConfigure;
import com.pakupaku.owner.controller.GetProfile;
import com.pakupaku.owner.controller.ProfileListener;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.helper.GlobalData;
import com.pakupaku.owner.helper.SharedHelper;
import com.pakupaku.owner.model.AuthToken;
import com.pakupaku.owner.model.Profile;
import com.pakupaku.owner.model.ServerError;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Utils;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pakupaku.owner.utils.TextUtils.isValidEmail;

public class LoginActivity extends AppCompatActivity implements ProfileListener {
    private String TAG = LoginActivity.class.getSimpleName();
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_password_eye_img)
    ImageView etPasswordEyeImg;
    @BindView(R.id.login_btn)
    Button loginBtn;
    private Context context;
    private Activity activity;
    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;
    boolean isInternetAvailable;
    private ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private String device_token = "";
    private String device_UDID = "";
    String email = "";
    String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = LoginActivity.this;
        activity = LoginActivity.this;
        connectionHelper = new ConnectionHelper(context);
        isInternetAvailable = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);
        getDeviceToken();
        etPasswordEyeImg.setTag(1);

    }

    @OnClick({R.id.et_password_eye_img, R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_password_eye_img:
                if (etPasswordEyeImg.getTag().equals(1)) {
                    etPassword.setTransformationMethod(null);
                    etPasswordEyeImg.setImageResource(R.drawable.ic_eye_close);
                    etPasswordEyeImg.setTag(0);
                } else {
                    etPassword.setTransformationMethod(new PasswordTransformationMethod());
                    etPasswordEyeImg.setImageResource(R.drawable.ic_eye_open);
                    etPasswordEyeImg.setTag(1);
                }
                break;
            case R.id.login_btn:
                validateLogin();
                // startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                break;
        }
    }

    private void validateLogin() {
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        if (email.isEmpty())
            Utils.displayMessage(activity, getResources().getString(R.string.please_enter_mail_id));
        else if (!isValidEmail(email))
            Utils.displayMessage(activity, getResources().getString(R.string.please_enter_valid_mail_id));
        else if (password.isEmpty())
            Utils.displayMessage(activity, getResources().getString(R.string.please_enter_password));
        else {
            if (isInternetAvailable) {
                HashMap<String, String> map = new HashMap<>();
                map.put("username", email);
                map.put("password", password);
                map.put("grant_type", "password");
                map.put("client_id", AppConfigure.CLIENT_ID);
                map.put("client_secret", AppConfigure.CLIENT_SECRET);
                map.put("device_type", "Android");
                map.put("device_token", device_token);
                map.put("device_id", device_UDID);
                /*String lang = "";
                if (LocaleUtils.getLanguage(this).equalsIgnoreCase("ja"))
                    lang = "Japanese";
                else lang = LocaleUtils.getLanguage(this);
                map.put("lang", lang);*/

                login(map);
            } else {
                Utils.displayMessage(activity, getResources().getString(R.string.oops_no_internet));
            }

        }
    }

    private void login(HashMap<String, String> map) {
        customDialog.show();
        Call<AuthToken> call = apiInterface.login(map);
        call.enqueue(new Callback<AuthToken>() {
            @Override
            public void onResponse(@NonNull Call<AuthToken> call, @NonNull Response<AuthToken> response) {
                if (response.isSuccessful()) {
                    SharedHelper.putKey(context, "access_token", response.body().getTokenType() + " " + response.body().getAccessToken());
                    GlobalData.accessToken = SharedHelper.getKey(context, "access_token");
                    new GetProfile(apiInterface, LoginActivity.this, LoginActivity.this);
                } else {
                    try {
                        ServerError serverError = new Gson().fromJson(response.errorBody().charStream(), ServerError.class);
                        if (serverError.getError().contains("incorrect")) {
                            Utils.displayMessage(activity, getString(R.string.invalid_credentials));
                        } else {
                            Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                        }

                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }

                    customDialog.dismiss();
                }
                //customDialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<AuthToken> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }

    @SuppressLint("HardwareIds")
    private void getDeviceToken() {
        try {
            device_UDID = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            Utils.print(TAG, "Device UDID:" + device_UDID);
        } catch (Exception e) {
            device_UDID = "COULD NOT GET UDID";
            e.printStackTrace();
            Utils.print(TAG, "Failed to complete device UDID");
        }

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                device_token = "";
                SharedHelper.putKey(context, "device_token", "" + device_token);
            } else {
                device_token = Objects.requireNonNull(task.getResult()).getToken();
                SharedHelper.putKey(context, "device_token", "" + device_token);
            }
        });
    }

    @Override
    public void onSuccess(Profile profile) {
        customDialog.dismiss();
        SharedHelper.putKey(context, "logged", "true");
        SharedHelper.putKey(context, "showlang", "true");
        GlobalData.profile = profile;
        startActivity(new Intent(context, HomeActivity.class));
        finish();
    }

    @Override
    public void onFailure(String error) {
        customDialog.dismiss();
        if (error.isEmpty())
            Utils.displayMessage(activity, getString(R.string.something_went_wrong));
        else Utils.displayMessage(activity, getString(R.string.something_went_wrong));
    }
}
