package com.pakupaku.owner.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pakupaku.owner.R;
import com.pakupaku.owner.controller.GetProfile;
import com.pakupaku.owner.controller.ProfileListener;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.helper.GlobalData;
import com.pakupaku.owner.helper.SharedHelper;
import com.pakupaku.owner.model.Profile;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Utils;

import java.util.Objects;

import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements ProfileListener {
    private Context context;
    private Activity activity;
    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;
    private ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private String device_token = "";
    private String device_UDID = "";
    private Utils utils = new Utils();
    private String TAG = "SplashActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splesh_screen);
        ButterKnife.bind(this);
        context = SplashActivity.this;
        activity = SplashActivity.this;
        connectionHelper = new ConnectionHelper(context);
        customDialog = new CustomDialog(context);

        GlobalData.accessToken = SharedHelper.getKey(context, "access_token");
        getDeviceToken();
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (SharedHelper.getKey(context, "logged").equalsIgnoreCase("true")
                    && SharedHelper.getKey(context, "logged") != null) {
                if (connectionHelper.isConnectingToInternet())
                    new GetProfile(apiInterface, SplashActivity.this, SplashActivity.this);
                else
                    Utils.displayMessage(SplashActivity.this, getString(R.string.oops_no_internet));
            } else {
                SharedHelper.putKey(SplashActivity.this, "showlang", "true");
                startActivity(new Intent(SplashActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        }, 3000);

    }

    @SuppressLint("HardwareIds")
    private void getDeviceToken() {
        try {
            device_UDID = android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            Utils.print(TAG, "Device UDID:" + device_UDID);
        } catch (Exception e) {
            device_UDID = "COULD NOT GET UDID";
            e.printStackTrace();
            Utils.print(TAG, "Failed to complete device UDID");
        }
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                device_token = "";
                SharedHelper.putKey(context, "device_token", "" + device_token);
                Log.w(TAG, "getInstanceId failed", task.getException());
            } else {
                device_token = Objects.requireNonNull(task.getResult()).getToken();
                SharedHelper.putKey(context, "device_token", "" + device_token);
            }
        });
    }

    @Override
    public void onSuccess(Profile profile) {
        GlobalData.profile = profile;
        startActivity(new Intent(context, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }

    @Override
    public void onFailure(String error) {
        customDialog.dismiss();
        if (error.isEmpty())
            Utils.displayMessage(activity, getString(R.string.something_went_wrong));
        else
            Utils.displayMessage(activity, getString(R.string.something_went_wrong));

        SharedHelper.putKey(context, "logged", "false");
        startActivity(new Intent(SplashActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}
