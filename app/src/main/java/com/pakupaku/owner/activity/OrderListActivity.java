package com.pakupaku.owner.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pakupaku.owner.R;
import com.pakupaku.owner.adapter.HistoryAdapter;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.model.ordersmodel.Orders;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Constants;
import com.pakupaku.owner.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListActivity extends AppCompatActivity {

    private String history_type = "";
    private String setHistoryType = "";
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.recycler_order_list)
    RecyclerView recyclerOrderList;
    @BindView(R.id.no_record_layout)
    RelativeLayout no_record_layout;
    private HistoryAdapter historyAdapter;
    private Context context;
    private Activity activity;
    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;
    boolean isInternetAvailable;
    private ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    List<Orders> ordersList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        ButterKnife.bind(this);
        backImg.setVisibility(View.VISIBLE);
        Intent intent = getIntent();
        history_type = intent.getStringExtra("history_type");
        assert history_type != null;
        if (history_type.equals(Constants.HISTORY_TYPE.ACCEPTED)) {
            title.setText(getString(R.string.accepted));
            setHistoryType = Constants.HISTORY_TYPE.ACCEPTED;
        } else if (history_type.equals(Constants.HISTORY_TYPE.SCHEDULED)) {
            title.setText(getString(R.string.scheduled));
            setHistoryType = Constants.HISTORY_TYPE.SCHEDULED;
        } else if (history_type.equals(Constants.HISTORY_TYPE.ONGOING)) {
            title.setText(getString(R.string.ongoing));
            setHistoryType = Constants.HISTORY_TYPE.ONGOING;
        }else if (history_type.equals(Constants.HISTORY_TYPE.PENDING)) {
            title.setText(getString(R.string.pending));
            setHistoryType = Constants.HISTORY_TYPE.PENDING;
        }else if (history_type.equals(Constants.HISTORY_TYPE.CANCELED)) {
            title.setText(getString(R.string.cancelled));
            setHistoryType = Constants.HISTORY_TYPE.CANCELED;
        }
        context = OrderListActivity.this;
        activity = OrderListActivity.this;
        connectionHelper = new ConnectionHelper(context);
        isInternetAvailable = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);

        setupAdapter();
        getOrdersList();
    }

    private void getOrdersList() {
        customDialog.show();
        Call<List<Orders>> call = apiInterface.getOrderList(setHistoryType);
        call.enqueue(new Callback<List<Orders>>() {
            @Override
            public void onResponse(@NonNull Call<List<Orders>> call, Response<List<Orders>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    ordersList = response.body();
                    if (!ordersList.isEmpty()) {
                        historyAdapter = new HistoryAdapter(ordersList, OrderListActivity.this);
                        recyclerOrderList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerOrderList.setHasFixedSize(true);
                        recyclerOrderList.setAdapter(historyAdapter);
                        no_record_layout.setVisibility(View.GONE);
                        recyclerOrderList.setVisibility(View.VISIBLE);
                    } else {
                        no_record_layout.setVisibility(View.VISIBLE);
                        recyclerOrderList.setVisibility(View.GONE);
                    }
                } else {
                    no_record_layout.setVisibility(View.VISIBLE);
                    recyclerOrderList.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Orders>> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }

    private void setupAdapter() {

    }

    @OnClick(R.id.back_img)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
