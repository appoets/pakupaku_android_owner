package com.pakupaku.owner.activity;

import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pakupaku.owner.R;
import com.pakupaku.owner.fragment.HomeFragment;
import com.pakupaku.owner.fragment.ProfileFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.main_container)
    FrameLayout mainContainer;
    @BindView(R.id.google_bottom)
    BottomNavigationView google_bottom;
    FragmentTransaction transaction;
    private Fragment fragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        // Set current item programmatically
        fragment = new HomeFragment();
        transaction.add(R.id.main_container, fragment).commit();
        google_bottom.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_home:
                    fragment = new HomeFragment();
                    break;
               /* case R.id.menu_notifiaction:
                    fragment = new NotificationFragment();
                    break;*/
                case R.id.menu_profile:
                    fragment = new ProfileFragment();
                    break;
            }
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment).commit();
            return true;
        });

    }
}
