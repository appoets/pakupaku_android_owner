package com.pakupaku.owner.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pakupaku.owner.R;
import com.pakupaku.owner.adapter.OrderProductAdapter;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.helper.GlobalData;
import com.pakupaku.owner.model.ordersmodel.Address;
import com.pakupaku.owner.model.ordersmodel.Invoice;
import com.pakupaku.owner.model.ordersmodel.Orders;
import com.pakupaku.owner.model.ordersmodel.User;
import com.pakupaku.owner.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrderDetailsActivity extends AppCompatActivity {
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.price_layout)
    RelativeLayout priceLayout;
    @BindView(R.id.user_img)
    CircleImageView userImg;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.address)
    TextView addressText;
    @BindView(R.id.payment_mode)
    TextView paymentMode;
    @BindView(R.id.user_details)
    LinearLayout userDetails;
    @BindView(R.id.price_txt)
    TextView priceTxt;
    @BindView(R.id.total_price)
    TextView totalPrice;
    @BindView(R.id.order_product_rv)
    RecyclerView orderProductRv;
    @BindView(R.id.linearLayout)
    LinearLayout linearLayout;
    @BindView(R.id.notes)
    TextView notes;
    @BindView(R.id.sub_total)
    TextView subTotal;
    @BindView(R.id.service_tax)
    TextView serviceTax;
    @BindView(R.id.promocode_amount)
    TextView promocodeAmount;
    @BindView(R.id.promocodeLayout)
    LinearLayout promocodeLayout;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.tv_cgst)
    TextView tvCgst;
    @BindView(R.id.tv_sgst)
    TextView tvSgst;
    @BindView(R.id.delivery_charges)
    TextView deliveryCharges;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.timesAgo)
    TextView times_ago;

    Orders order;
    Context context;
    Activity activity;
    ConnectionHelper connectionHelper;
    CustomDialog customDialog;
    boolean isInternet;
    OrderProductAdapter orderProductAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        context = OrderDetailsActivity.this;
        activity = OrderDetailsActivity.this;
        connectionHelper = new ConnectionHelper(context);
        isInternet = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);
        backImg.setVisibility(View.VISIBLE);
        priceLayout.setVisibility(View.GONE);
        times_ago.setVisibility(View.GONE);

        if (GlobalData.selectedOrder != null) {
            order = GlobalData.selectedOrder;
        } else {
            finish();
            return;
        }
        title.setText("#" + order.getId());
        if (order.getUser() != null) {
            User user = order.getUser();
            String name = Utils.toFirstCharUpperAll(user.getName());
            userName.setText(name);
            Glide.with(context).load(user.getAvatar()).apply(new RequestOptions().placeholder(R.drawable.ic_place_holder_image).error(R.drawable.ic_place_holder_image).dontAnimate()).into(userImg);
        }

        if (order.getAddress() != null) {
            Address address = order.getAddress();
            addressText.setText(address.getMapAddress());
        }

        if (order.getInvoice() != null) {
            Invoice invoice = order.getInvoice();
            String payment_mode = invoice.getPaymentMode();
            payment_mode = payment_mode.substring(0, 1).toUpperCase() + payment_mode.substring(1);
            paymentMode.setText(payment_mode);
            //int cgst_percentage_multiplayer = invoice.getCGST() / 100;
            //int sgst_percentage_multiplayer = invoice.getSGST() / 100;

            // int gross_amount = invoice.getGross() - invoice.getDiscount();

            //int cgst = (gross_amount * (cgst_percentage_multiplayer));
            //int sgst = (gross_amount * (sgst_percentage_multiplayer));

            subTotal.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/invoice.getGross());
            discount.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/invoice.getDiscount());
            // tvCgst.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/cgst);
            // tvSgst.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/sgst);
            deliveryCharges.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/invoice.getDeliveryCharge());
            total.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/invoice.getNet());
            serviceTax.setText(GlobalData.profile.getCurrency() + /*String.format("%.2f"*/invoice.getTax());
        /*if (invoice.getPromocodeAmount() > 0) {
            promocodeLayout.setVisibility(View.VISIBLE);
        } else {
            promocodeLayout.setVisibility(View.GONE);
        }
        promocodeAmount.setText(GlobalData.profile.getCurrency() + "-" +*//*String.format("%.2f"*//*(invoice.getPromocodeAmount()));*/
        } else {
            paymentMode.setText("Null");
        }

        if (order.getNote() != null)
            notes.setText(String.format("%s", order.getNote()));
        else
            notes.setText(getResources().getString(R.string.empty));
        orderProductAdapter = new OrderProductAdapter(context, order.getItems());
        orderProductRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        orderProductRv.setHasFixedSize(true);
        orderProductRv.setAdapter(orderProductAdapter);

    }

    @OnClick(R.id.back_img)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
