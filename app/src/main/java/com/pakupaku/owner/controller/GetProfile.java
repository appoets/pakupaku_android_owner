package com.pakupaku.owner.controller;

import android.content.Context;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pakupaku.owner.MyApplication;
import com.pakupaku.owner.helper.SharedHelper;
import com.pakupaku.owner.model.Profile;
import com.pakupaku.owner.model.ServerError;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Constants;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tamil on 3/16/2018.
 */

public class GetProfile {

    public GetProfile(ApiInterface apiInterface, Context context, final ProfileListener profileListener) {
        String device_id = Settings.Secure.getString(MyApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_type = "Android";
        String device_token = SharedHelper.getKey(context, "device_token");

        HashMap<String, String> params = new HashMap<>();
        params.put("device_id", device_id);
        params.put("device_type", device_type);
        params.put("device_token", device_token);
        Call<Profile> call = apiInterface.getProfile();
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
                if (response.isSuccessful()) {
                    SharedHelper.putKey(MyApplication.getInstance(), Constants.PREF.PROFILE_ID, "" + response.body().getId());
                    SharedHelper.putKey(MyApplication.getInstance(), Constants.PREF.CURRENCY, "" + response.body().getCurrency());
                    profileListener.onSuccess(response.body());
                } else try {
                    ServerError serverError = new Gson().fromJson(response.errorBody().charStream(), ServerError.class);
                    profileListener.onFailure(serverError.getError());
                } catch (JsonSyntaxException e) {
                    profileListener.onFailure("");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
                profileListener.onFailure("");
            }
        });
    }
}
