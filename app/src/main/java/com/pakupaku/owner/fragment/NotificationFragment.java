package com.pakupaku.owner.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pakupaku.owner.R;
import com.pakupaku.owner.adapter.NotificationAdapter;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.model.NotificationList;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    private NotificationAdapter notificationAdapter;
    private ArrayList<String> notificationList = new ArrayList<>();
    private Context context;
    private Activity activity;
    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;
    boolean isInternetAvailable;
    private ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

    public NotificationFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        title.setText(getString(R.string.notification));
        context = getActivity();
        activity = getActivity();
        connectionHelper = new ConnectionHelper(context);
        isInternetAvailable = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);

        for (int i = 0; i < 10; i++) {
            notificationList.add("Item" + i);
        }
        setupAdapter();
         getNotificationList();
        return view;
    }

    private void getNotificationList() {
        customDialog.setCancelable(false);
        customDialog.show();
        Call<List<NotificationList>> call = apiInterface.getNotifications();
        call.enqueue(new Callback<List<NotificationList>>() {
            @Override
            public void onResponse(Call<List<NotificationList>> call, Response<List<NotificationList>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()){
                    List<NotificationList> listsNotification=response.body();
                    notificationAdapter = new NotificationAdapter(listsNotification, getActivity());
                    recyclerList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    recyclerList.setHasFixedSize(true);
                    recyclerList.setAdapter(notificationAdapter);

                }
            }

            @Override
            public void onFailure(Call<List<NotificationList>> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }

    private void setupAdapter() {

    }

}
