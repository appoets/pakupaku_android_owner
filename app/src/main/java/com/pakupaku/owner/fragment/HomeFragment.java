package com.pakupaku.owner.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.pakupaku.owner.R;
import com.pakupaku.owner.activity.OrderListActivity;
import com.pakupaku.owner.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.click_accepted)
    LinearLayout clickAccepted;
    @BindView(R.id.click_scheduled)
    LinearLayout clickScheduled;
    @BindView(R.id.click_ongoing)
    LinearLayout clickOngoing;
    @BindView(R.id.click_pending)
    LinearLayout clickPending;
    @BindView(R.id.click_canceled)
    LinearLayout clickCanceled;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick({R.id.click_accepted, R.id.click_scheduled, R.id.click_ongoing, R.id.click_pending, R.id.click_canceled})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.click_accepted:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.ACCEPTED));
                break;
            case R.id.click_scheduled:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.SCHEDULED));
                break;
            case R.id.click_ongoing:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.ONGOING));
                break;
            case R.id.click_pending:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.PENDING));
                break;
            case R.id.click_canceled:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.CANCELED));
                break;
        }
    }

   /* @OnClick({R.id.click_upcomming, R.id.click_ongoing, R.id.click_canceled})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.click_upcomming:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.UP_COMING));
                break;
            case R.id.click_ongoing:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.ON_GOING));
                break;
            case R.id.click_canceled:
                startActivity(new Intent(getActivity(), OrderListActivity.class).putExtra("history_type", Constants.HISTORY_TYPE.CANCELED));
                break;
        }
    }*/
}
