package com.pakupaku.owner.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pakupaku.owner.R;
import com.pakupaku.owner.activity.ChangePasswordActivity;
import com.pakupaku.owner.activity.LoginActivity;
import com.pakupaku.owner.helper.ConnectionHelper;
import com.pakupaku.owner.helper.CustomDialog;
import com.pakupaku.owner.helper.GlobalData;
import com.pakupaku.owner.helper.SharedHelper;
import com.pakupaku.owner.model.ServerError;
import com.pakupaku.owner.network.ApiClient;
import com.pakupaku.owner.network.ApiInterface;
import com.pakupaku.owner.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    @BindView(R.id.title)
    TextView title;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    CustomDialog customDialog;
    ConnectionHelper connectionHelper;

    private Activity activity;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        activity = getActivity();
        customDialog = new CustomDialog(activity);
        connectionHelper = new ConnectionHelper(activity);
        return view;
    }

    @OnClick({R.id.click_change_pswd, R.id.click_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.click_change_pswd:
                activity.startActivity(new Intent(activity, ChangePasswordActivity.class));
                break;
            case R.id.click_logout:
                showLogoutAlertDialog();
                break;
        }
    }


    private void showLogoutAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.alert_log_out));
        builder.setPositiveButton(getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //  SharedHelper.putKey(activity, "showlang", "false");
                logOut();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void logOut() {
        customDialog.show();
        Call<ResponseBody> call = apiInterface.logOut();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    clearAndExit();
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(activity, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, activity.getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, activity.getString(R.string.something_went_wrong));
            }
        });
    }

    private void clearAndExit() {
        SharedHelper.clearSharedPreferences(activity);
        GlobalData.accessToken = "";
        SharedHelper.putKey(activity, "showlang", "true");
        activity.startActivity(new Intent(activity, LoginActivity.class));
        activity.finish();
    }
}
