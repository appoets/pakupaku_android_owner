package com.pakupaku.owner.network;

/**
 * Created by tamil@appoets.com on 30-08-2017.
 */


import com.pakupaku.owner.model.AuthToken;
import com.pakupaku.owner.model.ChangePassword;
import com.pakupaku.owner.model.NotificationList;
import com.pakupaku.owner.model.Profile;
import com.pakupaku.owner.model.ordersmodel.Orders;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/admin/oauth/token")
    Call<AuthToken> login(@FieldMap HashMap<String, String> params);

    @GET("api/admin/dispute-user")
    Call<Profile> getProfile();

    @FormUrlEncoded
    @POST("api/admin/password")
    Call<ChangePassword> changePassword(@FieldMap HashMap<String, String> params);

    @GET("api/admin/dispute-user")
    Call<ResponseBody> logOut();

    @GET("api/admin/orders")
    Call<List<Orders>> getOrderList(@Query("t") String params);

    @GET("api/admin/notification")
    Call<List<NotificationList>> getNotifications();

}
